from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from django.http import HttpRequest
from datetime import date
import unittest

# Create your tests here.

class Aps1UnitTest(TestCase):

    def test_Home_is_Exist(self):
        response = Client().get('/Home/')
        self.assertEqual(response.status_code, 200)
