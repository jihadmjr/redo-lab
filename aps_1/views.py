from django.shortcuts import render

# Create your views here.
my_name = "Caraval"

def index(request):
    response = {'name': my_name}
    return render(request, 'aps_1.html', response)
